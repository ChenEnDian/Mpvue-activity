let $vm
const plugin = {
  install (Vue) {
    if (!$vm) {
      $vm = Vue;
      Vue.prototype.alert = function(option){
        var o = {};
        if(typeof option =='string'){
          o = {
            content:option
          }
        }else if(typeof option =='object'){
          o = option;
        }else{
          Vue.$warn("alert anguments error!");
          return;
        }
        Vue.$vux.alert.show(Object.assign({
          title:"花伴商城",
        },o))
      }
      Vue.prototype.toast = function(option,position){
        if(typeof option =='string'){
          Vue.$vux.toast.text(option,position)
        }else if(typeof option =='object'){
          Vue.$vux.toast.show(option);
        }else{
          Vue.$warn("toast anguments error!");
        }
      }
      Vue.prototype.loading = function(option){
        if(typeof option =='string'){
          Vue.$vux.loading.show({
            text:option,
          })
        }else if(typeof option =='object'){
          Vue.$vux.loading.show(option);
        }else{
          Vue.$warn("loading anguments error!");
        }
      }
    }
  }
}
export default plugin
export const install = plugin.install
