export default {
  huakaExchangeVipMember: "huaka/exchangeVipMember", // 兑换码兑换会员
  couponGrabCoupons: "coupon/grabCoupons", // 优惠券拆分--领新人券
  userGetVipUserInfo: "user/getVipUserInfo", //获取花卡信息
  validateVipUser: 'vipActive/validateVipUser', // 查询是否是会员以及是否已经领取了优惠券
  exchangeTicket: 'activtyTicket/exchangeTicket', // 积分兑换优惠券接口
  grabCouponsByVipActivity: 'coupon/grabCouponsByVipActivity', // 会员领券的接口
  validateNewUserByActivity:"registerCoupon/validateNewUserByActivity",//判断新用户是否能领取，以及是否有领取过
  goodsQuery: "goods/query",
  bannerQuery: "banner/query",
  queryMainGoodsShelfInfo: 'shelf/queryMainGoodsShelfInfo',//首页查询所有货架
  goodsQueryOne: "goods/queryOne",//查询商品详情
  queryGoodsDetails: "goods/queryGoodsDetails",//查询商品详情（新规格）
  goodsStatisticsUpdateGoodsStatistics: "goodsStatistics/updateGoodsStatistics",
  userAddUserAddress: "user/addUserAddress",
  userQueryUserAddress: "user/queryUserAddress",
  userQueryUserAddressList: "user/queryUserAddressList",
  userDeleteUserAddress: "user/deleteUserAddress",
  userUpdateUserAddress: "user/updateUserAddress",
  loginLogin: "login/login",
  userQueryUserCredit: "zaCredit/queryCreditResult",
  orderUserDownOrder: "order/userDownOrder",
  orderUserOrders: "order/userOrders",
  orderQueryOrder: "order/queryOrder",
  loginGetAuthCode: "login/getAuthCode",
  catesearchByParentId: "category/searchByParentId",
  getBalance: 'account/getBalance', //余额查询
  findAccountTrades: 'account/findAccountTrades', //获取交易列表
  queryPartnerNo: 'order/queryPartnerNo', //商户号
  queryOneOrderNo: 'order/queryOneOrderNo',
  bannerqueryList: "banner/queryList", //lunbo
  categorySearchSelfByParentId: "category/searchSelfByParentId",//
  queryCategroyAll: "category/queryCategroyAll",//查询所有分类
  activeGoodsQueryActiveCouponById: "activeGoods/queryActiveCouponById", //获取活动是否结束
  couponGetCouponDetails: "coupon/getCouponDetails", //查询是否获取优惠券
  couponGrabCoupon: "coupon/grabCoupon",
  shelfQueryActiveGoodsPositionById: "shelf/queryActiveGoodsPositionById",
  couponGetEnableCouponNum: "coupon/getEnableCouponNum", //获取优惠券可以数量
  couponGetEnableCouponList: "coupon/getEnableCouponList", //获取可以使用优惠券
  couponFindCoupon: "coupon/findCoupon",
  searchGoods: "goods/queryGoodsInfoByName", //获取搜索商品
  shelfQueryTodaySellGoodsPositin: "shelf/queryTodaySellGoodsPositin", //获取首页今日特卖数据
  activeGoodsQueryActiveGoods: "activeGoods/queryActiveGoods", //	获取活动商品
  shelfQueryAllGoodsPositin: "shelf/queryAllGoodsPositin", //
  shelfQueryGoodsPositinById: "shelf/queryGoodsPositinById",
  activeGoodsQueryActiveGoodsById: "activeGoods/QueryActiveGoodsById",//查询活动商品详情
  queryActiveGoodsDetailsById: "activeGoods/queryActiveGoodsDetail",//查询活动商品详情(新规格)
  zaPayApplyCredit: "zaPay/applyCredit", //申请额度
  zaPaypplyPay: "zaPay/applyPay",
  zaPayResetPayPwd: "zaPay/resetPayPwd",
  zaPayBillList: "zaPay/billList",
  zaPayQuickRefund: "zaPay/quickRefund",
  zaPayMshCash: "zaPay/mshCash",
  orderOnfirmOrder: "order/confirmOrder",
  activeGoodsQueryActiveGoodsByActiveId: "activeGoods/queryActiveGoodsByActiveId",
  shareActiveGetLinkh5: "shareActive/getLink/h5",
  shelfFindShelfGoodsByPositionId: "shelf/findShelfGoodsByPositionId",
  shelfFindParentShelfGoodsByPositionId: 'shelf/findParentShelfGoodsByPositionId',
  shareActiveQueryUserInviteList: "shareActive/queryUserInviteList",
  orderGetOrder: "order/getOrder",
  orderListUserOrders: "order/listUserOrders",
  settingQueryOneByCode: "setting/queryOneByCode",
  shareActiveH5QueryCouponInfo: "shareActive/h5/queryCouponInfo",
  userQueryAddressInfo: "user/queryAddressInfo",
  getValidCode: 'activeCoupons/getValidCode', //获取活动短信验证码
  getBudgetAmount: 'goods/getBudgetAmount', //查询首付及分期金额
  queryGoodsInfoByName: 'goods/queryGoodsInfoByName',//查询商品 搜索
  getOpenId: "getXCXOpenId",
  realNameAuth: 'realNameAuth',  //实名认证
  querySupportBanks: 'querySupportBanks', //查询绑卡支持列表
  queryGoodsInfoByName: 'goods/queryGoodsInfoByName',//查询商品 搜索
  getNotReadMessageCount: 'messageUser/getNotReadMessageCount',//获取未读消息条数
  userMessages: "messageUser/userMessages",//分页获取用户的消息条目
  updateMsgState: "messageUser/updateMsgState",//标记已读
  getOpenId: "getXCXOpenId",
  mgmQueryRewardInfo: "mgm/queryRewardInfo",//获取奖励
  mgmQueryInviteInfo: "mgm/queryInviteInfo",//查询用户邀请人情况
  mgmQueryLatestInviteInfo: "mgm/queryLatestInviteInfo",//获取轮播邀请
  mgmBindInvite: "mgm/bindInvite",
  mgmQueryWxInfoByPhone: "mgm/queryWxInfoByPhone",//根据手机号查询用户信息
  mgmQueryCreditStatusForSDK: "mgm/queryCreditStatus",//获取用户是否授信
  mgmQueryMgmActives: "mgm/queryMgmActives",//获取活动商品信息
  userQueryUserForXcx: "user/queryUserForXcx",//
  loginLogout: "login/logout",
  placeOrder: "order/placeOrder",//2.3.0之后的用户下单接口
  payDownPayment: "order/payDownPayment",//首付支付
  queryCreditResult: 'zaCredit/queryCreditResult', //查询授信额度-------------
  queryBillList: 'zaRepay/queryBillList',//已出账or快速还款
  queryNotArrivedBill: 'zaRepay/queryNotArrivedBill',//未出账
  queryUserInfo: 'zaCredit/queryUserInfo',//(众安)查询用户信息
  userRepay: 'zaRepay/repay',//用户还款接口
  queryBasicInfo: 'zaRepay/queryBasicInfo',//用户额度及还款信息汇总查询
  queryBillInfo: 'zaRepay/queryBillInfo',//查询账单明细
  queryConsumeInfo: 'zaRepay/queryConsumeInfo',//查询用户消费信息
  queryRepaymentPlan: 'zaRepay/queryRepaymentPlan',//订单还款计划查询
  queryRepayInfo: 'zaRepay/queryRepayInfo',//还款信息查询接口
  grabCoupon: 'coupon/grabCoupon',//领取授信失败优惠券
  queryHome: 'home/queryHome',//3.0.1首页货架接口
  queryGroupGoods: 'home/queryGroupGoods',//查询商品组商品
  queryShelfDetail: 'home/queryShelfDetailForShelf',//查询货架详情
  queryShelfonegoods: 'home/queryShelfDetailForSingleProduct',//查询货架项单品详细信息
  wxShareCreateShareIcon: "wxShare/createShareIcon",
  queryGoodsList: 'goods/queryGoodsList',//3.0.2查询分类详情
  queryGoodsDetail: 'goods/queryGoodsDetail',//3.0.2 查询商品详情,
  getJdCategoryList: 'category/getJdCategoryList',//查询京东分类
  queryAllProvinceList: 'user/queryAllProvinceList',//查询京东地址省级
  queryCitysByProvinceId: 'user/queryCitysByProvinceId',//查询京东地址市级
  queryCountysByCityId: 'user/queryCountysByCityId',//查询京东地址区级
  queryTownsByCountyId: 'user/queryTownsByCountyId',//查询京东地址镇级
  wxShareCreateShareIcon: "wxShare/createShareIcon",//生成小程序分享图片
  wxShareGetShareInfo: "wxShare/getShareInfo",//获取小程序生成信息 其实可以做成统一的
  listUserAddress: 'user/listUserAddress',//3.0.2获取用户地址
  updateAddressState: "user/updateAddressState",//设置用户默认地址
  updateAddressIdCard: 'user/updateAddressIdCard',//设置地址身份证信息
  newplaceOrder: 'newOrder/placeOrder',//3.0.2 下单详情
  newPayment: 'newOrder/payment',//3.0.2 统一支付
  mshOrderTrial: 'newOrder/mshOrderTrial',//3.0.2msh试算
  checkOrder: 'newOrder/checkOrder', //3.0.2确认订单
  preTrade: 'newOrder/preTrade',//3.0.2 msh分期支付
  getFreight: "newOrder/getFreight",//运费计算
  getCashierDesk: 'newOrder/getCashierDesk',//获取收银台
  homeGetActivePush: "home/getActivePush",//获取首页弹窗
  queryUserInfo: 'zaCredit/queryUserInfo',      //查询用户信息
  modifyTradePassword: "zaCredit/modifyTradePassword",//修改交易密码(记得交易密码)
  resetTradePassword: "zaCredit/resetTradePassword",//重置交易密码(忘记交易密码)
  zhimaSecretkey: "zaCredit/zhimaSecretkey",//获取芝麻请求加密串
  zhimaResult: "zaCredit/zhimaResult",//芝麻信用信息回传
  setTradePassword: "zaCredit/setTradePassword",//设置交易密码
  faceRecognize: "zaCredit/faceRecognizeFile",//人脸识别
  queryCreditResult: "zaCredit/queryCreditResult",//查询授信额度
  applyCredit: "zaCredit/applyCredit",//申请授信
  realNameAuth: 'zaCredit/realNameAuth',         //实名认证
  ocrDistinguish: 'zaCredit/ocrDistinguish',    //OCR信息识别
  bindCardOTP: 'zaCredit/bindCardOTP',    //绑卡OTP 短信验证码
  bindCard: 'zaCredit/bindCard',          //绑卡
  querySupportBanks: 'zaCredit/querySupportBanks',   //查看银行支持列表
  queryCardBin: 'zaCredit/queryCardBin',            //查看银行卡Bin
  queryBindCardList: 'zaCredit/queryBindCardList',  //获取用户默认卡列表
  setDefaultCard: 'zaCredit/setDefaultCard',        //设置默认卡
  queryCreditResult: 'zaCredit/queryCreditResult', //查询授信额度
  collectContactInfo: 'zaCredit/collectContactInfo', //采集联系人信息
  preTrade: 'zaConsum/preTrade', //预授权消费
  consumeTrial: 'zaConsum/newConsumeTrial', //消费试算
  preTradeQuery: 'zaConsum/preTradeQuery', //预授权信息查询
  getZhimaInfo: "zaCredit/getZhimaInfo",//芝麻授权，获取授权基本信息
  sendZhimaSMS: "zaCredit/sendZhimaSMS",//发送芝麻授权验证码
  checkZhimaInfo: "zaCredit/checkZhimaInfo",//芝麻授权验证
  zaCreditTxFaceStart: "zaCredit/txFaceStart",//吊起活体检查小程序
  zaCreditTxFaceRecognize: "zaCredit/txFaceRecognize",//检查识别结果
  zaCreditQueryCarrierInfo: "zaCredit/queryCarrierInfo",//查询运营商信息
  zaCreditCreateCarrierTask: "zaCredit/createCarrierTask",//创建运营商密码采集任务
  zaCreditQueryCarrierTaskStatus: "zaCredit/queryCarrierTaskStatus",//查询运营商采集任务状态
  zaCreditResendCarrierVerifyCode: "zaCredit/resendCarrierVerifyCode",//运营商验证码重发
  zaCreditValidateCarrierVerifyCode: "zaCredit/validateCarrierVerifyCode",//查看验证码是否正确
  zaCreditSubmitCarrierTaskResult: "zaCredit/submitCarrierTaskResult",
  zaCreditResetCarrierPwd: "zaCredit/resetCarrierPwd",
  zaCreditValidateNewCarrierPwd: "zaCredit/validateNewCarrierPwd",
  mshOrderTrial: 'newOrder/mshOrderTrial',//3.0.2msh试算
  newOrderPayment:"newOrder/payment",//民生消费
  newOrderPreTrade:"newOrder/preTrade",//3.0.0下单接口
  
  
  userGetVipUserInfo:"user/getVipUserInfo",//获取花卡信息
  newOrderVipPay:"newOrder/vipPay",//购买花卡
  loginWxAuthLogin:"login/wxAuthLogin",
  huakaGetInviterInfo:"huaka/getInviterInfo",//查看用户开通花卡的状态
  huakaQueryHasKaInviteInfo:"huaka/queryHasKaInviteInfo",//查看邀请列表
  huakapplyReward:'huaka/applyReward',//邀请好友提现
  huakacopywriting:'huaka/copywriting',//邀请好友规则
  giftVipMember:"huaka/giftVipMember", // 领取会员状态（是否成功）
  listGiftVipMember:"huaka/listGiftVipMember", // 获取会员列表
  getMetaTitle: "huaka/giftNumber", // 获取metaTitle（领取会员）
  
  checkVipActiveGet: "huaka/checkVipActiveGet", // 判断是否领取会员成功
  getCoupon: 'coupon/getCoupon', // 获取用户优惠券详情
  getExtCouponInfo:'extension/getExtCouponInfo',//查询第三方券信息
  getInviterInfo:'extension/grabExtensionCoupon',//领券extension
  getEntrySingInfo: 'huaka/getEntrySingInfo',//获取分享加密字符串
}
