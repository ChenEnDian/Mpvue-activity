import Vue from 'vue';
import Vuex,{Store} from 'vuex';
import a from './modules/a';
import b from './modules/b';
import goodsIndex from './modules/goods'
import listAddress from './modules/me/listAddress'
import addAddress from './modules/me/addAddress'
import find from './modules/find/find'
import footer from './modules/footer'
Vue.use(Vuex);
let store = new Store({
	 modules: {
	    a,
	   	b,
		goodsIndex,
		listAddress,
		addAddress,
		find,
		footer
	  }
})
export default store
