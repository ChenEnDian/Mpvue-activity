import http from '@/util/http'
import Api from '@/util/api'
let state = {
  goodsInfo:{},
	toastFlag:false,
	showClassModal:false,
	mainAtta:[],
	otherAtta:[],
	selectIndex:0,
	selectNorm:{},
	showPresent:false,
	selectNum:1,
	showLoading:true,
	toastText:"",
  sid:0
  ,info: {}
};
let getters;
let mutations = {
	_setGoodsInfo(mstate,goodsInfo){
    if(mstate.sid){
      goodsInfo.normList.forEach((item,index)=>{
        if(item.normId == mstate.sid){
          mstate.selectNorm = item;
          mstate.selectIndex = index;
        }
      })
    }else{
      mstate.selectNorm = goodsInfo.normList[0];
    }
		mstate.goodsInfo = goodsInfo;
		mstate.showLoading = false;
	},
	_goodsNet(mstate,toastMessage){
		mstate.toastFlag = true;
		mstate.toastText = toastMessage;
		mstate.showLoading = false;
	},
	_changeClassModal(mstate,isShow){
		mstate.showClassModal = isShow;
	},
	_init(mstate){
		mstate = state;
	},
	_changeSelect(mstate,index){
		mstate.selectNorm = mstate.goodsInfo.normList[index];
		mstate.selectIndex = index;
	},
	_showresent(mstate,isShow){
		mstate.showPresent = isShow;
	},
	_setSelectNum(mstate,num){
		mstate.selectNum = num;
	}
}
let actions = {
	init({commit}){
		commit("_init");
	},
	getGoodsInfo({commit},goodsId){
		if (goodsId.isShlf) {
			http.post(Api.queryShelfonegoods,goodsId)
		.then((d)=>{
			if(d.code == 200){
				let {attaList} = d.data,mainAtta = [],otherAtta = [],goodsInfo={};
				goodsInfo = d.data;
				attaList.forEach((item)=>{
					if(item.attaType=='MAIN'){
						mainAtta.push(item);
					}else if(item.attaType=='OTHER'){
						otherAtta.push(item);
					}
				})
				goodsInfo.otherAtta = otherAtta;
				goodsInfo.mainAtta = mainAtta;
				commit("_setGoodsInfo",goodsInfo);
			}else{
				commit("_goodsNet",d.message);
			}
		})
		} else{
			http.post(Api.queryGoodsDetail,{id:goodsId})
		.then((d)=>{
			if(d.code == 200){
				let {attaList} = d.data,mainAtta = [],otherAtta = [],goodsInfo={};
				goodsInfo = d.data;
				attaList.forEach((item)=>{
					if(item.attaType=='MAIN'){
						mainAtta.push(item);
					}else if(item.attaType=='OTHER'){
						otherAtta.push(item);
					}
				})
				goodsInfo.otherAtta = otherAtta;
				goodsInfo.mainAtta = mainAtta;
				commit("_setGoodsInfo",goodsInfo);
			}else{
				commit("_goodsNet",d.message);
			}
		})
		}
		
	},
  setSelectIndex({state},sid){
    state.sid = sid;
  },
	changeModal({commit},isShow){
		commit("_changeClassModal",isShow);
	},
	changeSelect({commit},index){
		commit("_changeSelect",index);
	},
	changePresent({commit},isShow){
		commit("_showresent",isShow);
	},
	setSelectNum({commit},num){
		commit("_setSelectNum",num);
	}
};
function htmlParse(html){

}
export default {
  namespaced:true,
  state,
  getters,
  actions,
	mutations
}
