import http from '@/util/http'
import Api from '@/util/api'
import Vue from 'vue'
const merge = require('webpack-merge')
let state = {
	province: [],
	city: [],
	county: [],
	town: [],
	alartFlag:false,
	alertMsg:"",
	code: 0
};
let getters = {
	
};
let actions = {
	fetchprovince ({commit},params) {
		http.post(Api.queryAllProvinceList,{}).then((res) => {
			if(res.code == 200){
				commit('FetchProvince', res.data);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
		
	},
	
	fetchcity ({commit},params) {
		http.post(Api.queryCitysByProvinceId,params).then((res) => {
			if(res.code == 200){
				commit('FetchCity', res.data);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
	}
	,fetchcounty ({commit},params) {
		http.post(Api.queryCountysByCityId,params).then((res) => {
			if(res.code == 200){
				commit('FetchCounty', res.data);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
	}
	,fetchtown ({commit},params) {
		http.post(Api.queryTownsByCountyId,params).then((res) => {
			if(res.code == 200){
				commit('FetchTown', res.data);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
	}
};
let mutations = {

	FetchProvince (state,data) {
		state.province = data;
	},
	FetchCity (state, data) {
		state.city = [];
		for (let i = 0; i< data.length; i++) {
			state.city.push(merge({indexCity:i},data[i]));
		}
	}
	,FetchCounty (state, data) {
		state.county = [];
		for (let i = 0; i< data.length; i++) {
			state.county.push(merge({indexCounty:i},data[i]));
		}
	}
	,FetchTown(state, data) {
		state.town = [];
		if (!data) {
			return state.town = []; 
		}
		for (let i = 0; i< data.length; i++) {
			state.town.push(merge({indexTown:i},data[i]));
		}
	}
};

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}





