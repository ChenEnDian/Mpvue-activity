import http from '@/util/http'
import Api from '@/util/api'
import Vue from 'vue'
import utils from '@/util/util'
let userId = localStorage.getItem('userId') || '';
const merge = require('webpack-merge')
let state = {
	count: '0'
	,arr: []
	,creditAmount:{availableAmount: -1,temp:false,status: '',textstring:''}
	,firsT1and1Img: ''
	,banneR1and1: {}
	,banneR2and1: {}
	,firsT1and1: {}
	,firsT1and2: {}
	,firsT1and3: {}
	,twiN2and1: {}
	,twiN2and2: {}
	,recordListone: []
	,twiN2and3: {}
	,recordListtwo: []
};
let getters = {
	
};
let actions = {
	//获取消息条数
	getmessagecount ({commit},params) {
		http.post(Api.getNotReadMessageCount,params).then((res) => {
			if(res.code == 200){
				commit('GetMessageCount', res.data);
			}else{
				
				Vue.prototype.toast(res.message);
			}
		},false)
	}
	//获取find页面数据
	,queryhome ({commit}) {
		http.post(Api.queryHome,{}).then((res) => {
			if(res.code == 200){
				commit('QueryHome', res.data);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
	},
	setcreditAmount({commit},userInfo){
    if (!userInfo) { 
   	 commit('getCreditAmount',{temp: true,availableAmount: -1, status:0}) 	
    	 return;
    }
    http.post(Api.userQueryUserCredit,{
      phone: userInfo.user.accountName,
      uid: userInfo.user.id
    },false).then((d) => {
      if (d.code == 200) {
        var data =d.data;
        let availableAmount = null, textstring = null;
        availableAmount = data.creditAmount > 0 ? data.availableAmount : -1;
        textstring = utils.splitK((availableAmount.toFixed(2)));    
        commit('getCreditAmount',{ status: data.status||0, textstring: textstring, temp: true }) 	
      } else { 
      	commit('getCreditAmount',{ status: 0, temp: true } ) 
      }
    }).catch((e) => {
    	commit('getCreditAmount',{status: 0, temp: true }) 
    })
	}

};
let mutations = {
	GetMessageCount (state,data) {
		state.count = data;
	}
	,getCreditAmount (state,data){
		state.creditAmount =data ;
	}
	,QueryHome (state,data) {
		data.forEach((item, index) => {
			if (item.shelfType == "FIRST_SCREEN_BANNER_1_1") {//首屏1-1banner
				state.banneR1and1 = item;
		    } else if (item.shelfType == "FIRST_SCREEN_1_1") {//首屏1-1
		        state.firsT1and1 = item;
		        state.firsT1and1Img = item.shelfItemDtos[0]
		    } else if (item.shelfType == "FIRST_SCREEN_1_2") {//首屏1-2
		        state.firsT1and2 = item;
		    } else if (item.shelfType == "FIRST_SCREEN_1_3") {//首屏1-3
		        state.firsT1and3 = item;
		    } else if (item.shelfType == "TWO_SCREEN_2_1") {//二屏2-1
		        state.twiN2and1 = item;
		    } else if (item.shelfType == "TWO_SCREEN_2_2") {//二屏2-2
		        state.twiN2and2 = item;
		        http.post(Api.queryGroupGoods,{groupId: item.shelfItemDtos[0].groupId, numPerPage: 8, order: "asc", pageNum: 1}).then((res) => {
					if(res.code == 200){
						state.recordListone = res.data.rows;
					}else{
						Vue.prototype.toast(res.message);
					}
				})
		    } else if (item.shelfType == "TWO_SCREEN_BANNER_2_1") {//二屏-2-1banner
		        state.banneR2and1 = item;
		    } else if (item.shelfType == "TWO_SCREEN_2_3") {//二屏2-3
		        state.twiN2and3 = item;
		        http.post(Api.queryGroupGoods,{groupId: item.shelfItemDtos[0].groupId, numPerPage: 2, order: "asc", pageNum: 1}).then((res) => {
					if(res.code == 200){
						state.recordListtwo = res.data.rows;
					}else{
						Vue.prototype.toast(res.message);
					}
				})
		    }
		})
	}
	//上拉加载
	,loadmore (state,data) {
		state.recordListtwo = [...state.recordListtwo,...data.data];
		data.that.mescroll.endUpScroll(false)
		if (state.recordListtwo.length == data.total) {
			data.that.mescroll.endUpScroll(true);
			data.that.mescroll.lockUpScroll();
		}
		
	}
	,resetloadmore (state) {
		state.recordListtwo = []
	}
};

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}
