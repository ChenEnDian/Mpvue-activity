import Vue from 'vue'
import Router from 'vue-router'


const NeiGouHui = () => import('@/views/actives/20180408/index') //内购惠活动
const oldnewEnjoy = ()=>import('@/views/actives/20180410/index') //老的新人优享活动
const YmscJygb = ()=> import('@/views/actives/20180416/index') //20180416一抹色彩活动
//const WxActivity032201 = r => require.ensure([], () => r(require('@/views/actives/20180322/index')), 'WxActivity032201')
const WxActivity032201 = () => import('@/views/actives/20180322/index')//300元大礼包
//const WxActivity032601 = r => require.ensure([], () => r(require('@/views/actives/2018032601/index')), 'WxActivity032601')
const WxActivity032601 = () => import('@/views/actives/2018032601/index')//出游季

const replaceCoupon = () => import('@/views/actives/20180518/index')// 积分兑换优惠券
const invitation = () => import('@/views/actives/20180507/index') // 邀请会员列表
const anniversary = () => import('@/views/actives/20180510/index') //周年庆活动
const huacard = () => import('@/views/actives/20180508/index') //邀请好友激活花卡
const receiveVip = ()=> import('@/views/actives/20180503/index')//领取会员的活动
const ExchangeMembersLogin = ()=> import('@/views/actives/20180502/index') //用兑换码兑换会员
const ExchangeMembers = ()=> import('@/views/actives/20180502/ExchangeMembers')// 兑换花卡会员
const VipActivity = ()=> import('@/views/actives/20180509/index') // 会员活动页面
const getCoupons = ()=> import('@/views/actives/20180511/index')// 领取20元全品类优惠券
const Esteelauder = () => import('@/views/actives/20180515/index') //雅思兰黛活动
const newEnjoy = () => import('@/views/actives/20180516/index')// 优惠券拆分1000元优惠券
const The520 = () => import('@/views/actives/20180520/index') //520活动



//按需加载
//const Invite = r => require.ensure([], () => r(require('@/views/invite/invite')), 'Invite')
const Invite = () => import('@/views/invite/invite')
//const InviteGoods = r => require.ensure([], () => r(require('@/views/invite/inviteGoods')), 'InviteGoods')
const InviteGoods = () => import('@/views/invite/inviteGoods')
const find = () => import('@/views/find')

//const GoodsShelf = r => require.ensure([], () => r(require('@/views/find/goodsShelf')), 'goodsShelf')
const GoodsShelf = () => import('@/views/find/goodsShelf')
//const Message = r => require.ensure([], () => r(require('@/views/find/message')), 'Message')
const Message = () => import('@/views/find/message')
//const Goods = r => require.ensure([], () => r(require('@/views/goods')), 'goods')
const Goods = () => import('@/views/goods')
//const AddAddress = r => require.ensure([], () => r(require('@/views/me/addAddress')), 'AddAddress')
const AddAddress = () => import('@/views/me/addAddress')
//const ListAddress = r => require.ensure([], () => r(require('@/views/me/listAddress')), 'ListAddress')
const ListAddress = () => import('@/views/me/listAddress')
//const DiscountCoupon = r => require.ensure([], () => r(require('@/views/me/discountCoupon')), 'DiscountCoupon')
const DiscountCoupon = () => import('@/views/me/discountCoupon')


//const Installment = r => require.ensure([], () => r(require('@/views/me/installment')), 'Installment')
const Installment = () => import('@/views/me/installment')
//const Refresh = r => require.ensure([], () => r(require('@/views/me/refresh')), 'Refresh')
const Refresh = () => import('@/views/me/refresh')
//const ResetPassword = r => require.ensure([], () => r(require('@/views/me/resetPassword')), 'ResetPassword')
const ResetPassword = () => import('@/views/me/resetPassword')
//const Search = r => require.ensure([], () => r(require('@/views/classify/search')), 'Search')
const Search = () => import('@/views/classify/search')
//const Classify = r => require.ensure([], () => r(require('@/views/classify/classify')), 'Classify')
const Classify = () => import('@/views/classify/classify')
//const InstalmentDetail = r => require.ensure([], () => r(require('@/views/me/instalmentDetail')), 'InstalmentDetail')
const InstalmentDetail = () => import('@/views/me/instalmentDetail')
//const Me = r => require.ensure([], () => r(require('@/views/me/me')), 'Me')
const Me = () => import('@/views/me/me')
//const Order = r => require.ensure([], () => r(require('@/views/me/order')), 'Order')
const Order = () => import('@/views/me/order')
//const OrderInfo = r => require.ensure([], () => r(require('@/views/me/orderInfo')), 'OrderInfo')
const OrderInfo = () => import('@/views/me/orderInfo')
//const SubmitGoods = r => require.ensure([], () => r(require('@/views/goods/submitGoods')), 'SubmitGoods')
const SubmitGoods = () => import('@/views/goods/submitGoods')
//const SetletAddresslist = r => require.ensure([], () => r(require('@/views/goods/setletAddresslist')), 'SetletAddresslist')
const SetletAddresslist = () => import('@/views/goods/setletAddresslist')
//const Login = r => require.ensure([], () => r(require('@/views/me/login')), 'Login')
const Login = () => import('@/views/me/login')

//const SelectCoupon = r => require.ensure([], () => r(require('@/views/goods/selectCoupon')), 'SelectCoupon')
const SelectCoupon = () => import('@/views/goods/selectCoupon')
//const LinkType = r => require.ensure([], () => r(require('@/views/iframe/linkType')), 'LinkType')
const LinkType = () => import('@/views/iframe/linkType')
//yilei


//const MashanghuaIndex = r => require.ensure([], () => r(require('@/views/newPage/index')), 'MashanghuaIndex')
const MashanghuaIndex = () => import('@/views/newPage/index')
//const MashanghuaConfirm = r => require.ensure([], () => r(require('@/views/newPage/confirm')), 'MashanghuaConfirm')
const MashanghuaConfirm = () => import('@/views/newPage/confirm')
//const MashanghuaBindCard = r => require.ensure([], () => r(require('@/views/newPage/bindCard')), 'MashanghuaBindCard')
const MashanghuaBindCard = () => import('@/views/newPage/bindCard')
//const MashanghuaChooseBank = r => require.ensure([], () => r(require('@/views/newPage/chooseBank')), 'MashanghuaChooseBank')
const MashanghuaChooseBank = () => import('@/views/newPage/chooseBank')
//const MashanghuaContacts = r => require.ensure([], () => r(require('@/views/newPage/contacts')), 'MashanghuaContacts')
const MashanghuaContacts = () => import('@/views/newPage/contacts')
//const MashanghuaInfo = r => require.ensure([], () => r(require('@/views/newPage/info')), 'MashanghuaInfo')
const MashanghuaInfo = () => import('@/views/newPage/info')
//const MashanghuaCredit = r => require.ensure([], () => r(require('@/views/newPage/credit')), 'MashanghuaCredit')
const MashanghuaCredit = () => import('@/views/newPage/credit')
//const MashanghuaVeriface = r => require.ensure([], () => r(require('@/views/newPage/veriface')), 'MashanghuaVeriface')
const MashanghuaVeriface = () => import('@/views/newPage/veriface')
//const MashanghuaPassword = r => require.ensure([], () => r(require('@/views/newPage/password')), 'MashanghuaPassword')
const MashanghuaPassword = () => import('@/views/newPage/password')
//const MashanghuaDealStatus = r => require.ensure([], () => r(require('@/views/newPage/dealStatus')), 'MashanghuaDealStatus')
const MashanghuaDealStatus = () => import('@/views/newPage/dealStatus')

//支付中心
//const payCenter = r => require.ensure([], () => r(require('@/views/pay/payCenter')), 'payCenter')
const payCenter = () => import('@/views/pay/payCenter')
//const mshPay = r => require.ensure([], () => r(require('@/views/pay/mshPay')), 'mshPay')
const mshPay = () => import('@/views/pay/mshPay')
//const cmbcPay = r => require.ensure([], () => r(require('@/views/pay/cmbcPay')), 'cmbcPay')
const cmbcPay = () => import('@/views/pay/cmbcPay')


//支付管理
//const cmbcPaySucc = r => require.ensure([], () => r(require('@/views/payNotification/cmbcPaySucc')), 'cmbcPaySucc')
const cmbcPaySucc = () => import('@/views/payNotification/cmbcPaySucc')


Vue.use(Router)



export default new Router({
  routes: [{
		path: '/neigouhui',
		name: 'NeiGouHui',
		component: NeiGouHui,
		meta: {
			title: '内购惠'
		}
	},
	{
   	  path: '/activity/wxActivity032201',
      name: 'WxActivity032201',
      component: WxActivity032201,
      meta: {
        title: 'WxActivity032201'
      }
    },
  	{
		path: '/oldnewEnjoy',
		name: 'oldnewEnjoy',
		component: oldnewEnjoy,
		meta: {
			title: '新人优享'
		}
	},
	{
		path: '/ymscJygb',
		name: 'YmscJygb',
		component: YmscJygb,
		meta: {
			title: '一抹色彩，为你带来惊艳改变。全场商品199减10'
		}
	},
	{
      path: '/activity/wxActivity032601',
      name: 'WxActivity032601',
      component: WxActivity032601,
      meta: {
        title: '出行游'
      }
    },
	{
		path: '/invitation',
		name: 'invitation',
		component: invitation,
		meta: {
			title: '邀请好友'
		}
	},
	{
		path: '/receiveVip',
		name: 'receiveVip',
		component: receiveVip,
		meta: {
			title: '每天前二十位登录免费领取会员'
		}
	 },
	 {
		path: '/huacard',
		name: 'huacard',
		component: huacard,
		meta: {
			title: '我的花卡'
		}
	},
	{
		path: '/exchangeMembersLogin',
		name: 'ExchangeMembersLogin',
		component: ExchangeMembersLogin,
		meta: {
			title: '兑换码兑换会员登录界面'
		}
	},
	{
		path: '/exchangeMembers',
		name: 'ExchangeMembers',
		component: ExchangeMembers,
		meta: {
			title: '兑换码兑换会员'
		}
	},
	  	{
  		path: '/replaceCoupon',
		name: 'replaceCoupon',
		component: replaceCoupon,
		meta: {
			title: '积分兑换优惠券'
		}
  	},
	{
		path: '/anniversary',
		name: 'anniversary',
		component: anniversary,
		meta: {
			title: '周年庆'
		}
	},
  	{
  		path: '/newEnjoy',
		name: 'newEnjoy',
		component: newEnjoy,
		meta: {
			title: '优惠券拆分1000元'
		}
  	},
  	{
		path: '/VipActivity',
		name: 'VipActivity',
		component: VipActivity,
		meta: {
			title: '会员活动页面'
		}
	},
	{
		path: '/getCoupons',
		name: 'getCoupons',
		component: getCoupons,
		meta: {
			title: '领取优惠券'
		}
	},
  	{
    	path: '/mgm',
      name: 'Invite',
      component: Invite,
      meta: {
        title: '一元购'
      }
   },
   {
	path: '/Esteelauder',
	name: 'Esteelauder',
	component: Esteelauder,
	meta: {
		title: '时光献礼，唯你最美'
		}
	},
	{
	path: '/The520',
	name: 'The520',
	component: The520,
	meta: {
		title: '爱你就“购”了'
		}
	},
    {
    	path: '/mgm/inviteGoods',
      name: 'InviteGoods',
      component: InviteGoods,
      meta: {
        title: '一元购商品'
      }
    },
    {
    	path: '/find',
      name: 'find',
      component: find,
      meta: {
        title: '花伴'
      }
    },
    {
    	path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: '登录'
      }
    },
    {
    	path: '/add-address',
      name: 'AddAddress',
      component: AddAddress,
      meta: {
        title: '添加收货地址'
      }
    },
    {
    	path: '/list-address',
      name: 'ListAddress',
      component: ListAddress,
      meta: {
        title: '地址列表'
      }
    },
    {
    	path: '/discount-coupon',
      name: 'DiscountCoupon',
      component: DiscountCoupon,
      meta: {
        title: '优惠券'
      }
    },
    {
      path: '/refresh',
      name: 'Refresh',
      component: Refresh,
      meta: {
        title: '刷新页面'
      }
    },
    {
    	path: '/goods',
      name: 'goods',
      component: Goods,
      meta: {
        title: 'goods'
      }
    },
    {
    	path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword,
      meta: {
        title: '重置密码'
      }
    },
    {
    	path: '/search',
      name: 'Search',
      component: Search,
      meta: {
        title: '搜索'
      }
    },
    {
    	path: '/classify',
      name: 'Classify',
      component: Classify,
      meta: {
        title: '分类'
      }
    },
    {
    	path: '/installment',
      name: 'Installment',
      component: Installment,
      meta: {
        title: '支付'
      }
    },
    {
    	path: '/instalment-detail',
      name: 'InstalmentDetail',
      component: InstalmentDetail,
      meta: {
        title: '支付详情'
      }
    },
    {
    	path: '/me',
      name: 'Me',
      component: Me,
      meta: {
        title: '我的'
      }
    },
    {
      path: '/order',
      name: 'Order',
      component: Order,
      meta: {
        title: '订单'
      }
    },
    {
    	path: '/newPage/mashanghuaIndex',
      name: 'MashanghuaIndex',
      component: MashanghuaIndex,
      meta: {
        title: '马上花首页'
      }
    },
    {
      path: '/newPage/mashanghuaConfirm',
      name: 'MashanghuaConfirm',
      component: MashanghuaConfirm,
      meta: {
        title: '马上花信息确认'
      }
    },
    {
      path: '/newPage/mashanghuaBindCard',
      name: 'MashanghuaBindCard',
      component: MashanghuaBindCard,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaChooseBank',
      name: 'MashanghuaChooseBank',
      component: MashanghuaChooseBank,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaContacts',
      name: 'MashanghuaContacts',
      component: MashanghuaContacts,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaInfo',
      name: 'MashanghuaInfo',
      component: MashanghuaInfo,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaCredit',
      name: 'MashanghuaCredit',
      component: MashanghuaCredit,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaVeriface',
      name: 'MashanghuaVeriface',
      component: MashanghuaVeriface,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaPassword',
      name: 'MashanghuaPassword',
      component: MashanghuaPassword,
      meta: {
        title: '马上花'
      }
    },
    {
      path: '/newPage/mashanghuaDealStatus',
      name: 'MashanghuaDealStatus',
      component: MashanghuaDealStatus,
      meta: {
        title: '马上花'
      }
    }
    ,{
      path:"/pay/cmbcPaySucc",
      name:"cmbcPaySucc",
      component:cmbcPaySucc,
      meta:{
        title:"查询中..."
      }
    },{
      path:"/pay/cmbcPay",
      name:"cmbcPay",
      component:cmbcPay,
      meta:{
        title:"请稍等..."
      }
    },
    {
      path:"/pay/pay-center",
      name:"payCenter",
      component:payCenter,
      meta:{
        title:"支付"
      }
    }, {
      path: "/pay/msh-pay",
      name: "mshPay",
      component: mshPay,
      meta: {
        title: "支付"
      }
    },
    {
      path: "/goods-shelf", 
      name:"GoodsShelf",
      component:GoodsShelf,
      meta:{
        title:"货架"
      }
    },
    {
      path: "/order-info", 
      name:"OrderInfo",
      component:OrderInfo,
      meta:{
        title:"订单详情"
      }
    },
    {
      path: "/submit-goods", 
      name:"SubmitGoods",
      component:SubmitGoods,
      meta:{
        title:"确认订单"
      }
    },
    {
      path: "/setlet-addresslist", 
      name:"SetletAddresslist",
      component:SetletAddresslist,
      meta:{
        title:"选择地址"
      }
    },
    {
      path: "/select-coupon", 
      name:"SelectCoupon",
      component:SelectCoupon,
      meta:{
        title:"选择优惠券"
      }
    },
    {
      path: "/message", 
      name:"Message",
      component:Message,
      meta:{
        title:"消息中心"
      }
    },
    {
      path: "/link-type", 
      name:"LinkType",
      component:LinkType,
      meta:{
        title:"一抹色彩"
      }
    }
  ]
  ,scrollBehavior (to, from, savedPosition) {//保留滚动的位置
	  if (savedPosition) {
	    return savedPosition
	  } else {
	    return { x: 0, y: 0 }
	  }
	}
})
