import Vue from 'vue'
import App from './App'
import router from './router'
import store from './vuex/index'
import VueLazyload from 'vue-lazyload'
import VueScroller from 'vue-scroller'
import VueTouch from 'vue-touch'
import http from '@/util/http'
import '@/assets/less/main.less';
import '@/assets/less/index.less';
var Data = require('@/util/otoData');
Vue.use(VueScroller)
Vue.use(VueTouch, {name: 'v-touch'})



import { LoadingPlugin, ToastPlugin, AlertPlugin, ConfirmPlugin} from 'vux'
import vueLogger from 'vue-logger'
import mePlugin from '@/util/alertDialogToast'
import VConsole from 'vconsole';

Vue.use(VueLazyload);
Vue.use(LoadingPlugin);
Vue.use(ToastPlugin);
Vue.use(ConfirmPlugin);
Vue.use(AlertPlugin);
Vue.use(mePlugin);

Vue.prototype.post = http.post;

Vue.use(vueLogger, {
  prefix: "HuaBan-"+(+new Date()),
  dev: process.env.NODE_ENV === 'development',
  shortname: true,
  levels: ['warn']
})
Vue.filter("price",function(price){
  if (price){
    var nstr = (price / 100).toFixed(2);
    var fnum = nstr.split(".");
    var reg = /\d{1,3}(?=(\d{3})+$)/g;
    return fnum[0].replace(reg, '$&,')+ "." + fnum[1];
  }else{
    return "计算中"
  }
});

router.beforeEach((to, from, next) => {
/* 路由发生变化修改页面title */
  if (to.meta.title) {
      document.title = to.meta.title
  }
  if(to.query.isDebug || from.query.isDebug){
    if(!window.isDebug){
      window.isDebug = true;
      new VConsole();
    }
  }
  Vue.prototype.dataBind = process.env.NODE_ENV == 'production'? Data.productionData[to.path] : Data.developmentData[to.path];
  if(from.name == null && to.query.token){
      sessionStorage.setItem("plus_token",to.query.token);
  }
  next()
})



window._vue = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})